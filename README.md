# Almundo examen frontend

En este repositorio se encuentra el código para las dos aplicaciones que componen el examen para frontend de Almundo.

## Instrucciones para desplegar el aplicativo

* Descargar nodejs en su versión 9.3.0 o mayor e instalarlo.

* Clonar el repositorio e ingresar a la carpeta.

* Ejecutar el siguiente comando:

```
npm install
```
* una vez todos los modulos hallan sido instalados, ejecutar el siguiente comando:

```
node app.js
```

*Ingresar en el navegador a la dirección http://localhost:4040

## Comandos disponibles de webpack

Estos comandos son útiles para crear las recursos de css y de javascript en el cliente.

* Para crear los recursos en modo de desarrollo, ingresar el siguiente comando:

```
npm run start
```

* Para crear los recursos en modo de producción, ingresar el siguiente comando:

```
npm run build
```

* Para vigilar los cambios realizados a los archivos en tiempo real:

```
npm run watch
```

## Estructura del proyecto

* api: Carpeta donde se encuentra el controlador de Hotel.
* public: Carpeta donde se encuentran los archivos estáticos y recursos de la aplicación minificados.
* src: Recursos css y javascript que utiliza el cliente.
* storage: datos de los hoteles.
* Views: Carpeta en donde se encuentra la vista inicial del aplicativo
* app.js: archivo punto de entrada del aplicativo