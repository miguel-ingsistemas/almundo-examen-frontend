var fs = require('fs');

module.exports = {

	index: function(req, res){
		var hotels = JSON.parse(fs.readFileSync('storage/data.json', 'utf8'));

		var filtered_hotels = [];

		if(req.query.stars != null && req.query.name != null){
			for (var i = 0; i < hotels.length; i++) {
				if((hotels[i].stars == req.query.stars || req.query.stars == 0) && hotels[i].name.search(new RegExp(req.query.name, "i")) != -1){
					filtered_hotels.push(hotels[i]);
				}
			}
		}else if(req.query.stars != null){
			for (var i = 0; i < hotels.length; i++) {
				if((hotels[i].stars == req.query.stars || req.query.stars == 0)){
					filtered_hotels.push(hotels[i]);
				}
			}
		}else if(req.query.name != null){
			for (var i = 0; i < hotels.length; i++) {
				if(hotels[i].name.search(new RegExp(req.query.name, "i")) != -1){
					filtered_hotels.push(hotels[i]);
				}				
			}			
		}else{
			filtered_hotels = hotels;
		}

		filtered_hotels = filtered_hotels.splice(0, 10);

		var response = {
			"error": (filtered_hotels.length <= 0)? "No se encontraron hoteles":null,
			"hotels": filtered_hotels,
		};

		res.json(response);
	},

}