var express = require('express');
var app = express();
var bodyParser = require('body-parser');

var hotel = require('./api/Hotel');

app.set('view engine', 'ejs');
app.use(express.static('public'));

app.use(bodyParser.json());

//rutas
app.get('/', function(req, res) { res.render('index') });
app.get('/hotels', hotel.index);

app.listen(4040, function () { console.log('Almundo running on port 4040!') });