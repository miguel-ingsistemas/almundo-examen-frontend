(function() {
	angular
        .module('hotel-service', [])
        .factory('Hotel', ["$http", "$httpParamSerializer", hotel_factory]);

    function hotel_factory($http, $httpParamSerializer) {

        return {
            list:function(params, cb){
                console.log(params);
                $http({
                    method: 'GET',
                    url: 'hotels?'+$httpParamSerializer(params),
                }).then(function successCallback(response) {
                    if(response.data.error === null){
                        cb(null, response.data.hotels);
                    }else{
                        cb(response.data.error);                        
                    }
                })
            },
        }
    }

})();