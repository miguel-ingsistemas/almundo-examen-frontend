import angular from 'angular';
import './hotel.js';

import 'bootstrap/dist/css/bootstrap-reboot.min.css';
import 'bootstrap/dist/css/bootstrap-grid.min.css';
import './../css/styles.css';

(function() {
	angular
		.module('almundo', ['hotel-service'])
		.directive('hotel', hotel_directive)
		.controller('HotelController', ["Hotel", hotel_controller]);

	function hotel_directive(){
	    return {
	        restrict: 'E',
	        templateUrl: '../templates/hotel.html'
	    };
	}

	function hotel_controller(Hotel) {

	    var vm = this;

		vm.show_search = true;
		vm.show_stars = true;
		vm.show_filters = (screen.width < 576)?false:true;

	    vm.list = [];
	    vm.message = null;
	    vm.name = null;
	    vm.stars = [0,5,4,3,2,1];
       	vm.selected_star = 0;


	    vm.elements = function(num) {
		    return new Array(num);   
		}

		vm.search = function(){
			var params = {
				name: vm.name,
       			stars: vm.selected_star
			};

			Hotel.list(params, function(error, list){
		    	console.log(error);
		    	console.log(list);
		    	if(error === null){
		    		vm.message = null;
		    		vm.list = list;
		    	}else{
		    		vm.message = error;
		    		vm.list = [];
		    	}
		    });
		}
	    vm.search();
	}

})();
